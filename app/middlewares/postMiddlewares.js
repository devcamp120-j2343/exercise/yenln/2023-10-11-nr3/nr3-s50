const getAllPostsMiddlewares = (req, res, next) => {
    console.log("Get all Posts Middleware");

    next();
}

const createPostsMiddlewares = (req, res, next) => {
    console.log("Create Posts Middleware");

    next();
}

const getDetailPostsMiddlewares = (req, res, next) => {
    console.log("Get detail Post Middleware");

    next();
}

const updatePostsMiddlewares = (req, res, next) => {
    console.log("Update Posts Middleware");

    next();
}

const deletePostsMiddlewares = (req, res, next) => {
    console.log("Delete Posts Middleware");

    next();
}

module.exports = {
    getAllPostsMiddlewares,
    createPostsMiddlewares, 
    getDetailPostsMiddlewares,
    updatePostsMiddlewares,
    deletePostsMiddlewares
}