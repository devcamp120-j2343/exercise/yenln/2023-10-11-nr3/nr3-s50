const getAllCommentsMiddlewares = (req, res, next) => {
    console.log("Get all Comments Middleware");

    next();
}

const createCommentsMiddlewares = (req, res, next) => {
    console.log("Create Comments Middleware");

    next();
}

const getDetailCommentsMiddlewares = (req, res, next) => {
    console.log("Get detail Comment Middleware");

    next();
}

const updateCommentsMiddlewares = (req, res, next) => {
    console.log("Update Comments Middleware");

    next();
}

const deleteCommentsMiddlewares = (req, res, next) => {
    console.log("Delete Comments Middleware");

    next();
}

module.exports = {
    getAllCommentsMiddlewares,
    createCommentsMiddlewares, 
    getDetailCommentsMiddlewares,
    updateCommentsMiddlewares,
    deleteCommentsMiddlewares
}