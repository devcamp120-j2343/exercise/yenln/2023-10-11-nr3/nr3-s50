const mongoose = require("mongoose");

const postModel = require("../models/postModel")
const postRouter = require("../routes/postRouter");

const createPost = async (req, res) => {
    //Thao tác với CSDL
    const {
        userId,
        title,
        body
    } = req.body;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is invalid!"
        })
    }

    if (!title) {
        return res.status(400).json({
            message: "title khong hop le",
        });
    }
    if (!body) {
        return res.status(400).json({
            message: "body khong hop le",
        });
    }

    const newPost = {
        _id: new mongoose.Types.ObjectId(),
        userId,
        title,
        body
    }

    postModel.create(newPost)
        .then((data) => {
            return res.status(201).json({
                status: "Create new post sucessfully",
                data
            })
        })
        .catch((error) => {
            if (error.code === 11000) {
                return res.status(400).json({ message: "name already exists !" });
            } else {
                return res.status(500).json({
                    message: "Invalid error !",
                });
            }

        })

}

const getAllPost = (req, res) => {
    postModel.find()
        .then((data) => {
            if (data && data.length > 0) {
                return res.status(201).json({
                    status: "Get all posts sucessfully",
                    data
                })
            }
            else {
                return res.status(404).json({
                    status: "Not found any post",
                    data
                })

            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const getPostById = async (req, res) => {
    var postId = req.params.postId;

    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "postId is invalid!"
        })
    }

    postModel.findById(postId)
        .then((data) => {
            if (!data) {
                return res.status(404).json({
                    status: "Not found detail of this post",
                    data
                })
            }
            else {
                return res.status(201).json({
                    status: `Get post ${postId} sucessfully`,
                    data
                })

            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}

const updatePostById = async (req, res) => {
    //B1: thu thập dữ liệu
    const postId = req.params.postId;

    const {
        userId,
        title,
        body
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is invalid!"
        })
    }

    if (!title) {
        return res.status(400).json({
            message: "title khong hop le",
        });
    }
    if (!body) {
        return res.status(400).json({
            message: "body khong hop le",
        });
    }



    //B3: thực thi model
    try {
        let updatedPost = {

        }

        if (userId) {
            updatedPost.userId = userId;
        }
        if (title) {
            updatedPost.title = title;
        }
        if (body) {
            updatedPost.body = body;
        }

        const result = await postModel.findByIdAndUpdate(
            postId,
            updatedPost
        );

        if (result) {
            return res.status(200).json({
                status: "Update post sucessfully",
                data: updatedPost
            })
        } else {
            return res.status(404).json({
                status: "Not found any post"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const deletePostById = async (req, res) => {
    //B1: Thu thập dữ liệu
    var postId = req.params.postId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "postId is invalid!"
        })
    }

    try {
        const deletedPost = await postModel.findByIdAndDelete(postId);

        if (deletedPost) {
            return res.status(200).json({
                status: `Delete post ${postId} sucessfully`,
                data: deletedPost
            })
        } else {
            return res.status(404).json({
                status: "Not found any post"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {
    createPost,
    getAllPost,
    getPostById,
    updatePostById,
    deletePostById
}
