const photoModel = require("../models/photoModel");
const mongoose = require("mongoose");

const createPhoto = async (req, res) => {
    // B1 - collect data
    const { albumId, title, url, thumbnailUrl } = req.body;

    if (!mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            message: "album id invalid !",
        });
    }
    if (!title) {
        return res.status(400).json({
            message: "title invalid !",
        });
    }
    if (!url) {
        return res.status(400).json({
            message: "url invalid !",
        });
    }

    try {
        // B3 - save to database
        const result = await photoModel.create({
            albumId,
            title,
            url,
            thumbnailUrl,
        });
        return res.status(201).json({
            message: "Create photo successful !",
            data: result,
        });
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const getAllPhoto = async (req, res) => {
    try {
        const result = await photoModel.find();
        return res.status(200).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const getPhotoById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }

    try {
        const result = await photoModel.findById(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Successful !", data: result });
        } else {
            return res.status(404).json({
                message: "Couldn't find photo !",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const updatePhotoById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }
    const { albumId, title, url, thumbnailUrl } = req.body;
    if (albumId && !mongoose.Types.ObjectId.isValid(albumId)) {
        return res.status(400).json({
            message: "album id invalid !",
        });
    }
    if (title && title == "") {
        return res.status(400).json({
            message: "title invalid !",
        });
    }
    if (url && url == "") {
        return res.status(400).json({
            message: "url invalid !",
        });
    }

    let updatePhoto = {};
    if (albumId) updatePhoto.albumId = albumId;
    if (title) updatePhoto.title = title;
    if (url) updatePhoto.url = url;
    if (thumbnailUrl) updatePhoto.thumbnailUrl = thumbnailUrl;

    try {
        const result = await photoModel.findByIdAndUpdate(_id, updatePhoto, {
            new: true,
        });
        if (result) {
            return res.status(200).json({
                message: "Update Photo successful !",
                data: result,
            });
        } else {
            return res.status(404).json({
                message: "Couldn't update photo!",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const deletePhotoById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }
    try {
        const result = await photoModel.findByIdAndRemove(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Deleta photo successful !" });
        } else {
            return res.status(404).json({
                message: "Couldn't find photo",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

module.exports = {
    createPhoto,
    getAllPhoto,
    getPhotoById,
    deletePhotoById,
    updatePhotoById,
};
