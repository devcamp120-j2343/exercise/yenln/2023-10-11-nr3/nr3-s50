const albumModel = require("../models/albumModel");
const mongoose = require("mongoose");

const createAlbum = async (req, res) => {
    // B1 - collect data
    const { userId, title } = req.body;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "user id id invalid !",
        });
    }
    if (!title) {
        return res.status(400).json({
            message: "title invalid !",
        });
    }

    try {
        // B3 - save to database
        const result = await albumModel.create({ userId, title });
        return res.status(201).json({
            message: "Create Album successful !",
            data: result,
        });
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const getAllAlbum = async (req, res) => {
    try {
        const result = await albumModel.find();
        return res.status(200).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const getAlbumById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }

    try {
        const result = await albumModel.findById(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Successful !", data: result });
        } else {
            return res.status(404).json({
                message: "Couldn't find Album !",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const updateAlbumById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }
    const { userId, title } = req.body;
    if (userId && !mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "user id id invalid !",
        });
    }
    if (title && title == "") {
        return res.status(400).json({
            message: "title invalid !",
        });
    }

    let updateAlbum = {};
    if (userId) updateAlbum.userId = userId;
    if (title) updateAlbum.title = title;

    try {
        const result = await albumModel.findByIdAndUpdate(_id, updateAlbum, {
            new: true,
        });
        if (result) {
            return res.status(200).json({
                message: "Update Album successful !",
                data: result,
            });
        } else {
            return res.status(404).json({
                message: "Couldn't update Album!",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const deleteAlbumById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }
    try {
        const result = await albumModel.findByIdAndRemove(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Deleta Album successful !" });
        } else {
            return res.status(404).json({
                message: "Couldn't find Album",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

module.exports = {
    createAlbum,
    getAllAlbum,
    getAlbumById,
    deleteAlbumById,
    updateAlbumById,
};
