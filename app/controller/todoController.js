const todoModel = require("../models/todo.model");
const mongoose = require("mongoose");

const createTodo = async (req, res) => {
    // B1 - collect data
    const { userId, title, completed } = req.body;
    console.log(typeof completed);
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "user id id invalid !",
        });
    }
    if (!title) {
        return res.status(400).json({
            message: "title invalid !",
        });
    }
    if (!completed && typeof completed != "boolean") {
        return res.status(400).json({
            message: "completed invalid !",
        });
    }

    try {
        // B3 - save to database
        const result = await todoModel.create({ userId, title, completed });
        return res.status(201).json({
            message: "Create Todo successful !",
            data: result,
        });
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const getAllTodo = async (req, res) => {
    try {
        const result = await todoModel.find();
        return res.status(200).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const getTodoById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }

    try {
        const result = await todoModel.findById(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Successful !", data: result });
        } else {
            return res.status(404).json({
                message: "Couldn't find Todo !",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const updateTodoById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }
    const { userId, title, completed } = req.body;
    if (userId && !mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "user id id invalid !",
        });
    }
    if (title && title == "") {
        return res.status(400).json({
            message: "title invalid !",
        });
    }
    if (completed && typeof completed != "boolean") {
        return res.status(400).json({
            message: "completed invalid !",
        });
    }

    let updateTodo = {};
    if (userId) updateTodo.userId = userId;
    if (title) updateTodo.title = title;
    if (completed != undefined) updateTodo.completed = completed;

    try {
        const result = await todoModel.findByIdAndUpdate(_id, updateTodo, {
            new: true,
        });
        if (result) {
            return res.status(200).json({
                message: "Update Todo successful !",
                data: result,
            });
        } else {
            return res.status(404).json({
                message: "Couldn't update Todo!",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

const deleteTodoById = async (req, res) => {
    // B1 - Collect data
    const _id = req.params.id;
    if (!mongoose.Types.ObjectId.isValid(_id)) {
        return res.status(400).json({
            message: "Id invalid !",
        });
    }
    try {
        const result = await todoModel.findByIdAndRemove(_id);
        if (result) {
            return res
                .status(200)
                .json({ message: "Deleta Todo successful !" });
        } else {
            return res.status(404).json({
                message: "Couldn't find Todo",
            });
        }
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }
};

module.exports = {
    createTodo,
    getAllTodo,
    getTodoById,
    deleteTodoById,
    updateTodoById,
};
