const mongoose = require("mongoose");

const userModel = require("../models/userModel")
const userRouter = require("../routes/userRouter");

const createUser = async (req, res) => {
    const bodyData = req.query.bodyData;
    //Thao tác với CSDL
    const {
        name,
        username,
        address,
        phone,
        website,
        company
    } = req.body;

    if (!name) {
        return res.status(400).json({
            message: "name khong hop le",
        });
    }
    if (!username) {
        return res.status(400).json({
            message: "username khong hop le",
        });
    }
    if (!phone || isNaN(phone)) {
        return res.status(400).json({
            message: "phone khong hop le",
        });
    }
    if (!website) {
        return res.status(400).json({
            message: "website khong hop le",
        });
    }


    const newUser = {
        _id: new mongoose.Types.ObjectId(),
        name,
        username,
        address,
        phone,
        website,
        company  
    }

    userModel.create(newUser)
        .then((data) => {
            return res.status(201).json({
                status: "Create new user sucessfully",
                data
            })
        })
        .catch((error) => {
            if (error.code === 11000) {
                return res.status(400).json({ message: "name already exists !" });
            } else {
                return res.status(500).json({
                    message: "Invalid error !",
                });
            }

        })

}

const getAllUser = (req, res) => {
    userModel.find()
        .then((data) => {
            if (data && data.length > 0) {
                return res.status(201).json({
                    status: "Get all users sucessfully",
                    data
                })
            }
            else {
                return res.status(404).json({
                    status: "Not found any user",
                    data
                })

            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const getUserById = async (req, res) => {
    var userId = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is invalid!"
        })
    }

    userModel.findById(userId)
        .then((data) => {
            if (!data) {
                return res.status(404).json({
                    status: "Not found detail of this user",
                    data
                })
            }
            else {
                return res.status(201).json({
                    status: `Get user ${userId} sucessfully`,
                    data
                })

            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}

const updateUserById = async (req, res) => {
    //B1: thu thập dữ liệu
    const userId = req.params.userId;

    const {
        name,
        username,
        address,
        phone,
        website,
        company
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!name) {
        return res.status(400).json({
            message: "name khong hop le",
        });
    }
    if (!username) {
        return res.status(400).json({
            message: "username khong hop le",
        });
    }
    if (!phone || isNaN(phone)) {
        return res.status(400).json({
            message: "phone khong hop le",
        });
    }
    if (!website) {
        return res.status(400).json({
            message: "website khong hop le",
        });
    }


    //B3: thực thi model
    try {
        let updatedUser = {

        }

        if (name) {
            updatedUser.name = name;
        }
        if (username) {
            updatedUser.username = username;
        }
        if (address) {
            updatedUser.address = address;
        }
        if (phone) {
            updatedUser.phone = phone;
        }
        if (website) {
            updatedUser.website = website;
        }
        if (company) {
            updatedUser.company = company;
        }

        const result = await userModel.findByIdAndUpdate(
            userId,
            updatedUser
        );

        if (result) {
            return res.status(200).json({
                status: "Update user sucessfully",
                data: updateduser
            })
        } else {
            return res.status(404).json({
                status: "Not found any user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const deleteUserById = async (req, res) => {
    //B1: Thu thập dữ liệu
    var userId = req.params.userId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is invalid!"
        })
    }

    try {
        const deletedUser = await userModel.findByIdAndDelete(userId);

        if (deletedUser) {
            return res.status(200).json({
                status: `Delete user ${userId} sucessfully`,
                data: deletedUser
            })
        } else {
            return res.status(404).json({
                status: "Not found any user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
}
