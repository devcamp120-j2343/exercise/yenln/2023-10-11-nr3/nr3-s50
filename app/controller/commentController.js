const mongoose = require("mongoose");

const commentModel = require("../models/commentModel")
const commentRouter = require("../routes/commentRouter");

const createComment = async (req, res) => {
    //Thao tác với CSDL
    const {
        postId,
        name,
        email,
        body
    } = req.body;

    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "postId is invalid!"
        })
    }

    const newcomment = {
        _id: new mongoose.Types.ObjectId(),
        postId,
        name,
        email,
        body
    }

    commentModel.create(newcomment)
        .then((data) => {
            return res.status(201).json({
                status: "Create new comment sucessfully",
                data
            })
        })
        .catch((error) => {
            if (error.code === 11000) {
                return res.status(400).json({ message: "name already exists !" });
            } else {
                return res.status(500).json({
                    message: "Invalid error !",
                });
            }

        })

}

const getAllComment = async (req, res) => {
    try {
        const result = await commentModel.find();
        return res.status(200).json({ message: "Successful !", data: result });
    } catch (error) {
        return res.status(500).json({
            message: "Invalid error !",
        });
    }


}

const getCommentById = async (req, res) => {
    var commentId = req.params.commentId;

    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "commentId is invalid!"
        })
    }

    commentModel.findById(commentId)
        .then((data) => {
            if (!data) {
                return res.status(404).json({
                    status: "Not found detail of this comment",
                    data
                })
            }
            else {
                return res.status(201).json({
                    status: `Get comment ${commentId} sucessfully`,
                    data
                })

            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })
}

const updateCommentById = async (req, res) => {
    //B1: thu thập dữ liệu
    const commentId = req.params.commentId;

    const {
        postId,
        name,
        email,
        body
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(postId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "postId is invalid!"
        })
    }

    //B3: thực thi model
    try {
        let updatedComment = {

        }

        if (postId) {
            updatedComment.postId = postId;
        }
        if (name) {
            updatedComment.name = name;
        }
        if (email) {
            updatedComment.email = email;
        }

        const result = await commentModel.findByIdAndUpdate(
            commentId,
            updatedComment
        );

        if (result) {
            return res.status(200).json({
                status: "Update comment sucessfully",
                data: updatedComment
            })
        } else {
            return res.status(404).json({
                status: "Not found any comment"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const deleteCommentById = async (req, res) => {
    //B1: Thu thập dữ liệu
    var commentId = req.params.commentId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(commentId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "commentId is invalid!"
        })
    }

    try {
        const deletedcomment = await commentModel.findByIdAndDelete(commentId);

        if (deletedcomment) {
            return res.status(200).json({
                status: `Delete comment ${commentId} sucessfully`,
                data: deletedcomment
            })
        } else {
            return res.status(404).json({
                status: "Not found any comment"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {
    createComment,
    getAllComment,
    getCommentById,
    updateCommentById,
    deleteCommentById
}
