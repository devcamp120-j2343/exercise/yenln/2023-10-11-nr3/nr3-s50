const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const photoSchema = new Schema({
    albumId: {
        type: mongoose.Types.ObjectId,
        ref: "Album",
    },
    title: {
        type: String,
        required: true,
    },
    url: {
        type: String,
        required: true,
    },
    thumbnailUrl: {
        type: String,
    },
});

module.exports = mongoose.model("Photo", photoSchema);
