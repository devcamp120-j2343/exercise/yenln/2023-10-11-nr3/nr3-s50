const express = require("express");
const router = express.Router();

const {
    createTodo,
    getAllTodo,
    getTodoById,
    deleteTodoById,
    updateTodoById,
} = require("../controllers/todo.controller");

router.get("/", getAllTodo);
router.get("/:id", getTodoById);
router.post("/", createTodo);
router.put("/:id", updateTodoById);
router.delete("/:id", deleteTodoById);

module.exports = router;
