const express = require("express");
const router = express.Router();

const albumController= require("../controller/albumController");

router.get("/", albumController.getAllAlbum);

router.get("/:id", albumController.getAlbumById);

router.post("/", albumController.createAlbum);

router.put("/:id", albumController.updateAlbumById);

router.delete("/:id", albumController.deleteAlbumById);

module.exports = router;
