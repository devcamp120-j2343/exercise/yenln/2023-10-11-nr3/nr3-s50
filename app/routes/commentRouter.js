//import { Express } from "express";
const express = require("express");

const commentMiddleware = require("../middlewares/commentMiddlewares");

const commentController = require("../controller/commentController")

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});
 
router.get("/",commentMiddleware.getAllCommentsMiddlewares, commentController.getAllComment)

router.comment("/",commentMiddleware.createCommentsMiddlewares, commentController.createComment)

router.get("/:commentId",commentMiddleware.getDetailCommentsMiddlewares, commentController.getCommentById)

router.put("/:commentId",commentMiddleware.updateCommentsMiddlewares, commentController.updateCommentById)

router.delete("/:commentId",commentMiddleware.deleteCommentsMiddlewares, commentController.deleteCommentById)

module.exports = router;