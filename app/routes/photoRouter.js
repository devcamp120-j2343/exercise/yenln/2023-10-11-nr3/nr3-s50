const express = require("express");
const router = express.Router();

const photoController = require("../controller/photoController");

router.get("/", photoController.getAllPhoto);
router.get("/:id", photoController.getPhotoById);
router.post("/", photoController.createPhoto);
router.put("/:id", photoController.updatePhotoById);
router.delete("/:id", photoController.deletePhotoById);

module.exports = router;
