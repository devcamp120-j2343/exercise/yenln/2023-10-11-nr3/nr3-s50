//import { Express } from "express";
const express = require("express");

const postMiddleware = require("../middlewares/postMiddlewares");

const postController = require("../controller/postController")

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});
 
router.get("/",postMiddleware.getAllPostsMiddlewares, postController.getAllPost)

router.post("/",postMiddleware.createPostsMiddlewares, postController.createPost)

router.get("/:postId",postMiddleware.getDetailPostsMiddlewares, postController.getPostById)

router.put("/:postId",postMiddleware.updatePostsMiddlewares, postController.updatePostById)

router.delete("/:postId",postMiddleware.deletePostsMiddlewares, postController.deletePostById)

module.exports = router;