// Câu lệnh này tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const express = require("express");

// Import mongooseJS
const mongoose = require("mongoose");

const userModel = require("./app/models/userModel");
const postModel = require("./app/models/postModel");
const commentModel = require("./app/models/commentModel");

// Khởi tạo app express
const app = express();

// Khai báo middleware đọc json
app.use(express.json());

// Khai báo middleware đọc dữ liệu UTF-8
app.use(express.urlencoded({
    extended: true
}))

// Khai báo cổng của project
const port = 8000;

mongoose.connect("mongodb://localhost:27017/Zigvy_Interview", (err) => {
    if(err) {
        throw err;
    }

    console.log("Connect MongoDB successfully!");
})

var userRouter = require("./app/routes/userRouter");
var postRouter = require("./app/routes/postRouter");
var commentRouter = require("./app/routes/commentRouter");
var photoRouter = require("./app/routes/photoRouter");
var todoRouter = require("./app/routes/todoRouter");
var albumRouter = require("./app/routes/albumRouter");

// app.use((request, response, next) => {
//     console.log("Time", new Date());
//     next();
// })

// app.use((request, response, next) => {
//     console.log("Request method: ", request.method);
//     next();
// })

app.use((request, response, next) => {
    console.log("Time", new Date());
    next();
},
(request, response, next) => {
    console.log("Request method: ", request.method);
    next();
}
)

// Khai báo API dạng get "/" sẽ chạy vào đây
// Callback function: Là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/", (request, response) => {
    let today = new Date();

    response.status(200).json({
        message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

app.use("/api/v1/users", userRouter);
app.use("/api/v1/post", postRouter);
app.use("/api/v1/comment", commentRouter);
app.use("/api/v1/photo", photoRouter);
app.use("/api/v1/todo", todoRouter);
app.use("/api/v1/album", albumRouter);

// Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng) " + port);
})